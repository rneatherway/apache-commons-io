package utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;

// some changes
public class UtilityClassesExample {
    public static void main(String[] args) throws IOException {
        File file1 = new File("src/main/resources/file1.txt");
        File file2 = new File("src/main/resources/file3.html");
        FileUtils.copyURLToFile(new URL("https://www.nixsolutions.com"), file2);
        newMethod();
    }

    private static void oldMethod() throws IOException {
        try (InputStream in = new URL("https://www.nixsolutions.com").openStream()) {
            InputStreamReader inR = new InputStreamReader(in);
            BufferedReader buf = new BufferedReader(inR);
            String line;
            while ((line = buf.readLine()) != null) {
                System.out.println(line);
            }
        }
    }

    private static void newMethod() throws IOException {
        try (InputStream in = new URL("https://www.nixsolutions.com").openStream()) {
            System.out.println(IOUtils.toString(in, StandardCharsets.UTF_8));
        }
    }

    private static void testFile(File file) {
        try (InputStream in = new FileInputStream(file)) {
            String str = IOUtils.toString(in, StandardCharsets.UTF_8);
            System.out.println(str);
            in.close();
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean oldEqualsTest(InputStream one, InputStream two) throws IOException {
        boolean result = true;
        int data1;
        int data2;
        System.out.println(one.available());
        System.out.println(two.available());
        if (one.available() != two.available()) {
            return false;
        }
        while ((data1 = one.read()) != -1) {

        }
        return result;
    }

    private static void oldCopyFile(File srcFile, File destFile){
        try (BufferedInputStream bis = new BufferedInputStream(
                new FileInputStream(srcFile));
             BufferedOutputStream bos = new BufferedOutputStream(
                     new FileOutputStream(destFile))) {
            int symbol;
            while ((symbol = bis.read()) != -1) {
                bos.write(symbol);
            }
        } catch (FileNotFoundException ex) {
            throw new IllegalArgumentException("This file does not exist", ex);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void newCopyFile(File srcFile, File destFile) throws IOException {
        FileUtils.copyFile(srcFile, destFile);

        File dir = new File("/some/dir");
        File file = new File("file.txt");
        //List<String> lines = FileUtils.readLines(file, StandardCharsets.UTF_8);

        // We can check if a file exists somewhere inside a certain directory.
        System.out.println(FileUtils.directoryContains(dir, file));

    }
}
